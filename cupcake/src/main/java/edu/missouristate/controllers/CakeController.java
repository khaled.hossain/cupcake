package edu.missouristate.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CakeController {

	@GetMapping(path="/")
	public String getIndex() {
		return "cup.jsp";
	}
	
}
